<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class Sendsmsrecords extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        // create the table
        $table = $this->table('sms_records',array('engine'=>'InnoDB'));
        $table->addColumn('platform_id', 'integer',array('limit' => 11,'comment'=>'短信平台id'))
            ->addColumn('phone', 'string',array('limit' => 11,'comment'=>'接收短信手机号码'))
            ->addColumn('expiry_time', 'integer',array('limit' => 11,'comment'=>'过期时间'))
            ->addColumn('sms_content', 'string',array('limit' => 255,'default'=>'','comment'=>'短信内容'))
            ->addColumn('status', 'integer',array('limit' => MysqlAdapter::INT_TINY,'default'=>1,'comment'=>'短信平台状态，1未过期，2已过期'))
            ->addColumn('create_time', 'integer',array('limit' => 11,'default'=>0,'comment'=>'创建时间'))
            ->setPrimaryKey('id')
            ->setComment('短信发送记录表')
            ->create();
    }

    /**
     * 提供回滚的删除用户表方法
     */
    public function down(){
        $this->dropTable('sms_records');
    }
}
