<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Smstemplate extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        // create the table
        $table = $this->table('sms_tpl',array('engine'=>'InnoDB'));
        $table->addColumn('platform_id', 'integer',array('limit' => 11,'comment'=>'短信平台id'))
            ->addColumn('type', 'string',array('limit' => 20,'default'=>'register','comment'=>'模板类型(register：注册,login：登录,reset_password：重置密码,forget_password：忘记密码)'))
            ->addColumn('template_name', 'string',array('limit' => 100,'default'=>'','comment'=>'模版名称'))
            ->addColumn('template_code', 'string',array('limit' => 100,'default'=>'','comment'=>'模版CODE'))
            ->addColumn('template_content', 'string',array('limit' => 255,'default'=>'','comment'=>'模板内容'))
            ->addColumn('create_time', 'integer',array('limit' => 11,'default'=>0,'comment'=>'创建时间'))
            ->addColumn('update_time', 'integer',array('limit' => 11,'default'=>NULL,'comment'=>'更新时间','null'=>true))
            ->addColumn('delete_time', 'integer',array('limit' => 11,'default'=>NULL,'comment'=>'删除时间','null'=>true))
            ->setPrimaryKey('id')
            ->setComment('短信模板')
            ->create();
    }

    /**
     * 提供回滚的删除用户表方法
     */
    public function down(){
        $this->dropTable('sms_tpl');
    }
}
