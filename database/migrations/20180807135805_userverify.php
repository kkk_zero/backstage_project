<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Userverify extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        // create the table
        $table = $this->table('users_verify',array('engine'=>'InnoDB'));
        $table->addColumn('user_id', 'integer',array('limit' => 11,'comment'=>'用户id'))
            ->addColumn('username', 'string',array('limit' => 15,'comment'=>'用户名，登陆使用'))
            ->addColumn('salt', 'string',array('limit' => 16,'comment'=>'密码盐'))
            ->addColumn('password', 'string',array('limit' => 32,'comment'=>'密码盐加密后的密码'))
            ->addColumn('register_time', 'integer',array('limit' => 11,'comment'=>'注册时间'))
            ->addColumn('is_delete', 'boolean',array('limit' => 1,'default'=>0,'comment'=>'删除状态，1已删除'))
            ->addIndex(array('username'), array('unique' => true))
            ->addTimestamps()//默认生成create_time和update_time两个字段
            ->setPrimaryKey('id')
            ->setComment('用户验证数据表')
            ->create();
    }

    /**
     * 提供回滚的删除用户表方法
     */
    public function down(){
        $this->dropTable('users_verify');
    }
}
