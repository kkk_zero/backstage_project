<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Usertoken extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        // create the table
        $table = $this->table('users_token',array('engine'=>'InnoDB'));
        $table->addColumn('user_id', 'integer',array('limit' => 11,'comment'=>'用户id'))
            ->addColumn('token', 'string',array('limit' => 32,'comment'=>'token'))
            ->addColumn('expiry_time', 'string',array('limit' => 11,'comment'=>'过期时间'))
            ->addColumn('ip', 'string',array('limit' => 50,'comment'=>'ip地址'))
            ->addColumn('user_agent', 'string',array('limit' => 150,'comment'=>'用户代理标识'))
            ->addColumn('device_id', 'string',array('limit' => 50,'comment'=>'设备id'))
            ->addColumn('status', 'boolean',array('limit' => 1,'default'=>0,'comment' => '状态，1正常，2过期'))
            ->addColumn('create_time', 'integer',array('limit' => 11,'default'=>0,'comment' => '创建时间'))
            ->setComment('用户token表')
            ->create();
    }

    /**
     * 提供回滚的删除用户表方法
     */
    public function down(){
        $this->dropTable('users_token');
    }
}
