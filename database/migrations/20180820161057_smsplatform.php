<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class Smsplatform extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        // create the table
        $table = $this->table('sms_platform',array('engine'=>'InnoDB'));
        $table->addColumn('platform_name', 'string',array('limit' => 11,'comment'=>'短信平台名称'))
            ->addColumn('app_id', 'string',array('limit' => 100,'default'=>'','comment'=>'短信平台appid'))
            ->addColumn('app_key', 'string',array('limit' => 100,'default'=>'','comment'=>'短信平台appkey'))
            ->addColumn('app_signature', 'string',array('limit' => 255,'default'=>'','comment'=>'短信平台签名'))
            ->addColumn('status', 'integer',array('limit' => MysqlAdapter::INT_TINY,'default'=>1,'comment'=>'短信平台状态，1启用，2禁用'))
            ->addColumn('create_time', 'integer',array('limit' => 11,'default'=>0,'comment'=>'创建时间'))
            ->addColumn('update_time', 'integer',array('limit' => 11,'default'=>NULL,'comment'=>'更新时间','null'=>true))
            ->addColumn('delete_time', 'integer',array('limit' => 11,'default'=>NULL,'comment'=>'删除时间','null'=>true))
            ->setPrimaryKey('id')
            ->setComment('短信模板')
            ->create();
    }

    /**
     * 提供回滚的删除用户表方法
     */
    public function down(){
        $this->dropTable('sms_platform');
    }


}
