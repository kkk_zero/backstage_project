<?php

use think\migration\Seeder;
use app\common\model\SmsPlatform;
use app\common\model\SmsTpl;

class Sms extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $platform = [
            [
                'platform_name' => '阿里大鱼',
                'app_id'        => 'LTAIDBxxywcXDae0',
                'app_key'       => 'Q9vT3XAfW2axVrLAgGaUJBgQVuENYA',
                'app_signature' => '阿里大鱼',
                'status'        => '1',
                'create_time'   => time(),
                'update_time'   => time(),
            ]
        ];

        $tpl = [
            [
                [
                    'platform_id'       => '',
                    'type'              => 'register',
                    'template_name'     => '注册',
                    'template_code'     => 'SMS_142384629',
                    'template_content'  => '您正在申请手机注册，验证码为：${code}，5分钟内有效！',
                    'create_time'       => time(),
                    'update_time'       => time(),
                ],
                [
                    'platform_id'       => '',
                    'type'              => 'login',
                    'template_name'     => '登录',
                    'template_code'     => 'SMS_142389606',
                    'template_content'  => '验证码为：${code}，您正在登录，若非本人操作，请勿泄露。',
                    'create_time'       => time(),
                    'update_time'       => time(),
                ],
                [
                    'platform_id'       => '',
                    'type'              => 'reset_password',
                    'template_name'     => '重置密码',
                    'template_code'     => 'SMS_142949875',
                    'template_content'  => '您的动态码为：${code}，您正在进行密码重置操作，如非本人操作，请忽略本短信！',
                    'create_time'       => time(),
                    'update_time'       => time(),
                ],
                [
                    'platform_id'       => '',
                    'type'              => 'forget_password',
                    'template_name'     => '忘记密码',
                    'template_code'     => 'SMS_142384630',
                    'template_content'  => '您的动态码为：${code}，您正在进行密码重置操作，如非本人操作，请忽略本短信！',
                    'create_time'       => time(),
                    'update_time'       => time(),
                ]

            ]

        ];

        foreach ($platform as $pk => $pv){
            $sms_platform = new SmsPlatform();
            $platform_id = $sms_platform->insertGetId($pv);
            $tpl_data = $tpl[$pk];
            foreach ($tpl_data as $tpl_k => $tpl_v){
                $tpl_v['platform_id'] = $platform_id;
                $tpl_data[$tpl_k] = $tpl_v;
            }
            $sms_tpl = new SmsTpl();
            $sms_tpl->insertAll($tpl_data);
        }


    }
}