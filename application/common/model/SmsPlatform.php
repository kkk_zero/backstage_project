<?php

namespace app\common\model;

use think\Model;

class SmsPlatform extends Model
{
    /**
     * 表名
     * @var string
     */
    protected $name = 'sms_platform';

    /**
     * 设置redis
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function set_redis()
    {
        $data = $this->select();
        $redis = new \Redis();
        $redis->set(SMS_PLATFORM_LIST,$data);
        return $data;
    }

}
