<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 2018/8/7
 * Time: 23:05
 */

namespace app\common\validate;

use think\Validate as ThinkValidate;

class User extends ThinkValidate
{
    /**
     * 规则
     * @var array
     */
    protected $rule = [
        'username'      =>  'require|max:11',
        'password'      =>  'require|length:8,16',
        'repassword'    =>  'require|length:8,16',
        'verify_code'   =>  'require|length:4',
        'device_id'     =>  'require',
    ];

    /**
     * 规则提示
     * @var array
     */
    protected $message = [
        'username.require'      =>  '用户名不为空',
        'username.max'          =>  '用户名长度不能超过11个字符',
        'password.require'      =>  '密码不为空',
        'password.length'       =>  '密码长度为8-16位',
        'repassword.require'    =>  '确认密码不为空',
        'repassword.length'     =>  '确认密码长度为8-16位',
        'verify_code.require'   =>  '验证码不为空',
        'verify_code.length'    =>  '验证码长度为4位',
        'device_id.require'     =>  '设备号不为空',
    ];

    /**
     * 规则场景
     * @var array
     */
    protected $scene = [
        'register'  =>  ['username','password','repassword','verify_code','device_id'],
        'login'     =>  ['username','password','device_id'],
    ];
}