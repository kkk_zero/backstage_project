<?php

/**
 * 文件名：Verify.php
 * 创建日期：2018/8/30
 * 创建时间：10:21
 * 创建者：kun
 **/

/**
 * 验证
 */

namespace app\common\logic;


use app\common\model\SmsRecords;

class Verify
{
    /***
     * 生成手机验证码
     * @param $phone            手机号码
     * @param $platform_id      平台id
     * @return bool|string
     */
    public static function code($phone, $platform_id)
    {
        // 获取生成的验证码
        $code = auto_code();

        $array = [
            'platform_id'   => $platform_id,
            'phone'         => $phone,
            'expiry_time'   => time() + config('sms')['expiry_time'],
            'sms_content'   => $code,
            'status'        => 1,
            'create_time'   => time()
        ];

        $sms_records = new SmsRecords();
        $sms_records_id = $sms_records->insertGetId($array);
        if ($sms_records_id){
            return [
                'code'              => $code,
                'sms_records_id'    => $sms_records_id
            ];
        }else{
            return false;
        }

    }

    /**
     * 验证验证码
     * @param $phone        手机号码
     * @param $code         验证码
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function check_code($phone,$code)
    {
        $sms_records = new SmsRecords();
        $where = [
            'phone'         => $phone,
            'sms_content'   => $code
        ];
        $result = $sms_records->where($where)->find();
        if ($result){
            if ($result['expiry_time'] < time()){

            }else{
                error('','');
            }
        }else{
            error();
        }
    }

}