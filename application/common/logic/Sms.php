<?php

/**
 * 文件名：Sms.php
 * 创建日期：2018/8/28
 * 创建时间：13:54
 * 创建者：kun
 **/

namespace app\common\logic;
use app\common\model\SmsPlatform;
use kkk\sms\AlidayuSms;


class Sms
{

    /**
     * 短信验证码
     * @param $tel              手机号码
     * @param $business_type    业务类型，register注册，login登录，forget_password忘记密码，reset_password重置密码
     */
    public function send($phone,$business_type){
        // 查询号码是否是黑名单

        // 获取设定的短信平台
        // 1、阿里大鱼   2、阿里云片
        $sms_platform = config('platform');

        // 获取验证码
        $code = Verify::code($phone,$sms_platform);

        // 获取短信模板
        $template_code = $this->get_template_code($sms_platform,$business_type);

        switch ($sms_platform){
            default:
            case 1:
                $alidy = new AlidayuSms();
                $content = $alidy->send_sms_code($tel, $code, $template_code);
        }
        return $content;
    }

    /**
     * 获取短信平台模板代码
     * @param $platform
     * @param $business_type
     */
    public function get_template_code($platform,$business_type)
    {
        $sms_platform = redis_connent()->get(SMS_PLATFORM_LIST);
        if ($sms_platform){
            $sms_platform = new SmsPlatform();
            $sms_platform = $sms_platform->set_redis();
        }




    }

}