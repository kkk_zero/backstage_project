<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 2018/8/28
 * Time: 22:44
 */

namespace app\common\logic;

use JPush\Client as JPushClient;


class Jpush
{
    protected $app_key;

    protected $master_secret;

    protected $JGpush;

    public function __construct($option = [])
    {
        $jpush_config = config('jpush');
        isset($option['app_key']) ? $this->app_key = $option['app_key'] : $jpush_config['app_key'];
        isset($option['master_secret']) ? $this->master_secret = $option['master_secret'] : $jpush_config['master_secret'];
        $this->JGpush = new JPushClient($this->app_key,$this->master_secret);
        $this->production = true;
    }

    /**
     * 推送别名
     */
    public function pushAlias($alias,$title,$content,$extras,$sendTime='')
    {
        $response = $this->JGpush->push()
            ->setPlatform('all')
            ->addAlias($alias)                      //别名
            ->message($content, array(
                'title' => $title,
                'extras' => $extras,
            ))
            ->setNotificationAlert($title)
            ->iosNotification($content, array(
                'category' => 'iOS category',
                'extras' => $extras,
            ))
            ->androidNotification($content, array(
                'title' => $title,
                'extras' => $extras
            ))
            ->options(array(
                'time_to_live'=>3600,
                'apns_production' => $this->production,
            ))
            ->send();

        $res = $this->response($response);

    }

    /**
     * 推送全部
     * @param $msg
     * @param null $extras
     * @param null $title
     */
    public function pushAll($title,$content,$extras,$sendTime='')
    {
        try {
            $builder = $this->JGpush->push()
                ->setPlatform('all')
                ->setAudience('all')
                ->setNotificationAlert($title)
                ->iosNotification($content, array(
                    'category' => 'iOS category',
                    'extras' => $extras,
                ))
                ->androidNotification($content, array(
                    'title' => $title,
                    'extras' => $extras
                ))
                ->message($content, array(
                    'title' => $title,
                    'extras' => $extras,
                ))
                ->options(array(
                    'time_to_live'=>3600,
                    'apns_production' => $this->production,
                ));
            if($sendTime==''){
                $response=$builder->send();
//                p($builder->send());
            }else{
                $builder = $builder->build();
                $response=$this->JGpush->schedule()->createSingleSchedule("定时发送的任务", $builder, array("time"=>$sendTime));
            }
        } catch (\Exception $e) {
            $response= $e;
        }
        $this->response($response);
    }

    /**
     * 推送
     * @param $title            标题
     * @param $content          内容
     * @param $extras           附加字段
     * @param string $sendTime  定时发送时间
     * @param array $audience   推送目标
     * $audience = [
    'tag'               => [],
    'tag_and'           => [],
    'alias'             => [],
    'registration_id'   => [],
    ];
     */
    public function push($title,$content,$extras,$platform = 'all',$audience = 'all',$sendTime='')
    {
        try {
            $platform = count($platform) > 0 ? $platform : 'all';
            $audience = count($audience) > 0 ? $audience : 'all';
            $builder = $this->JGpush->push();
            $builder->setPlatform($platform);
            $builder->setAudience($audience);
            $builder->setNotificationAlert($title);
            if (in_array('ios',$platform) || $platform == 'all'){
                $builder->iosNotification($content, array(
                    'category' => 'iOS category',
                    'extras' => $extras,
                ));
            }
            if (in_array('android',$platform) || $platform == 'all') {
                $builder->androidNotification($content, array(
                    'title' => $title,
                    'extras' => $extras
                ));
            }
            $builder->message($content, array(
                'title' => $title,
                'extras' => $extras,
            ));
            $builder->options(array(
                'time_to_live'=>3600,
                'apns_production' => $this->production,
            ));
            if($sendTime == ''){
                $response = $builder->send();
            }else{
                $builder = $builder->build();
                $response=$this->JGpush->schedule()->createSingleSchedule("定时发送的任务", $builder, array("time"=>$sendTime));
            }
        } catch (\Exception $e) {
            $response= $e;
        }
        $this->response($response);
    }

    public function response($response = NULL)
    {
        if ($response == '' || $response = NUll ){
            return false;
        }

        if (isset($response['http_code']) && $response['http_code'] == 200) {
            return true;
        } else {
            return false;
        }
    }
}