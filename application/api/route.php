<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

//Route::rule('hello/:name', function ($name) {
//    return 'Hello,' . $name . '!';
//});


Route::group('auth',[
    'login'   => ['Auth/login', ['method' => 'post']],
    'register'   => ['Auth/register', ['method' => 'post']],
]);

Route::group('system',[
    'upload'   => ['System/upload', ['method' => 'post']],                      // 上传文件
]);

Route::group('common',[
    'get_sms_dcode'   => ['Common/get_sms_dcode', ['method' => 'post']],        // 获取验证码
    'register'   => ['Common/register', ['method' => 'post']],                  // 注册
    'login'   => ['Common/login', ['method' => 'post']],                        // 登录
]);