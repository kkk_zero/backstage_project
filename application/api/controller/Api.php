<?php
/**
 * Api基础控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\api\controller;

use think\exception\ValidateException;
use think\Loader;
use think\Request;
use think\Response;

class Api
{
    //Request实例
    protected $request;

    //请求参数
    protected $param;

    //token
    protected $token;

    //是否需要验证token
    protected $needAuth = true;

    //当前请求用户id,默认0
    protected $uid = 0;

    //验证失败是否抛出异常
    protected $failException = false;

    //是否批量验证
    protected $batchValidate = false;

    public function __construct(Request $request = null)
    {
        if (is_null($request)) {
            $request = Request::instance();
        }
        $this->request = $request;
        $this->param   = $request->param();
        $this->token   = $this->request->header('AppAuthorization');


        //如果当前需要验证token
        if (true == $this->needAuth) {
            $this->uid = check_login();
        }
    }




    //访问空页面
    public function _empty()
    {
        return error('Api not found');
    }


    //参考tp自带Controller  validate
    protected function validateFailException($fail = true)
    {
        $this->failException = $fail;
        return $this;
    }

    //参考tp自带Controller  validate
    protected function validate($data, $validate, $message = [], $batch = false, $callback = null)
    {
        if (is_array($validate)) {
            $v = Loader::validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                list($validate, $scene) = explode('.', $validate);
            }
            $v = Loader::validate($validate);
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }
        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        if (is_array($message)) {
            $v->message($message);
        }

        if ($callback && is_callable($callback)) {
            call_user_func_array($callback, [$v, &$data]);
        }

        if (!$v->check($data)) {
            if ($this->failException) {
                throw new ValidateException($v->getError());
            } else {
                return $v->getError();
            }
        } else {
            return true;
        }
    }
}