<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 2018/8/7
 * Time: 22:58
 */

namespace app\api\controller;


use app\common\logic\Sms;
use think\Request;
use app\common\logic\User;

class Common extends Api
{
    // 关闭token验证
    protected $needAuth = false;

    public function __construct(Request $request = null)
    {
        parent::__construct($request);

    }

    /**
     * 注册
     * @return mixed
     */
    public function register()
    {
        // 验证数据
        $result = $this->validate($this->param,'User.register');
        if(true!==$result){
            return $this->error($result);
        }

        // 验证验证码

        // 注册逻辑
        $user_logic = new User();
        $user_logic->register($this->param);

    }

    /**
     * 获取验证码
     */
    public function get_sms_code(){
        $phone = '13129225731';
        $business_type = '1';
        $sms_logic = new Sms();
        $sms_logic->send($phone,$business_type);
    }

    /**
     * 登录
     * @return mixed
     */
    public function login()
    {
        // 验证数据
        $result = $this->validate($this->param,'User.login');
        if(true!==$result){
            return $this->error($result);
        }

        // 登录逻辑
    }

}