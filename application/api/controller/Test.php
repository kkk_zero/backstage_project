<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 2018/8/21
 * Time: 22:57
 */

namespace app\api\controller;


use app\index\controller\Controller;
use mikkle\tp_alipay\Alipay;

class Test extends Controller
{

    public function alipay()
    {
//        echo Alipay::instance(config('alipay'))->PagePay()->setParam([
//            "return_url"=>"http://frame.17dang.net/api/test/notify",
//            "notify_url"=>"http://frame.17dang.net/api/test/request"
//        ])
//            ->setBizContentParam([
//                "subject"=>"debug",
//                "out_trade_no"=>(string)time(),
//                "total_amount"=>"0.01",
//            ])
//            ->getQuickPayUrl();

        $alipay = Alipay::instance(config('alipay'))->WapPay()->setParam([
            "return_url"=>"http://frame.17dang.net/api/test/notify",
            "notify_url"=>"http://frame.17dang.net/api/test/request",
            "quit_url"=>"http://frame.17dang.net/api/test/request"
        ])->setBizContentParam([
            "subject"=>"debug",
            "out_trade_no"=>(string)time(),
            "total_amount"=>"0.01",
        ])->getQuickPayUrl();
       p($alipay);
    }

    public function notify()
    { 

    }

    public function request()
    {

    }


}