<?php

/**
 * 文件名：System.php
 * 创建日期：2018/8/22
 * 创建时间：9:15
 * 创建者：kun
 **/



namespace app\api\controller;

use app\common\model\Attachments;
use app\index\controller\Controller;
use think\Request;

class System extends Controller
{

    protected $needAuth = true;

    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }

    //上传文件
    public function upload()
    {
        $model = new Attachments();
        $file       = $model->upload('file');
        if ($file) {
            return success('上传成功');
        }
        return error($model->getError());

    }


}

